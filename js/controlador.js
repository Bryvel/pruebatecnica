const pruebaTecnica = new Vue({
    el: "#pruebaTecnica",
    data: {
        dataHeros: {},
        name:"",
        age:"",
        secretIdentity:"",
        powers:[],
        power:""
    
    },
    mounted() {
        this.initApp();
    },
    methods: {

        initHeros: async function (url) {
            // Uso de AXIOS es una libreria facil rapida e intuitiva, su desventaja es limitada en comparasion al fetch
            const heros = await axios.get(url);
            return heros;
        },
        initApp: async function () {
            const rawData = await this.initHeros("https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json")
            this.dataHeros= rawData.data;
        },
        deleteHero: function(index){
            this.dataHeros.members.splice(index,1)
            alert("Heroe Eliminado");
        },
        agregarPoder: function(){
            this.powers.push(this.power)
            this.power="";
            alert("Poder Agregado");
        },
        createHero: function(){
            let hero={
                name:this.name,
                age:this.age,
                secretIdentity:this.secretIdentity,
                powers:this.powers,
            }
            alert("Heroe Creado");
            this.dataHeros.members.push(hero);
            this.name="";
            this.age="";
            this.secretIdentity="";
            this.powers=[];
        },
        updateHero: function(index){
            this.dataHeros.members[index].name=this.name;
            this.dataHeros.members[index].age=this.age;
            this.dataHeros.members[index].secretIdentity=this.secretIdentity;
            this.dataHeros.members[index].powers=this.powers;
            alert("Heroe Modificado");
            this.name="";
            this.age="";
            this.secretIdentity="";
            this.powers=[];
        }
    }


})